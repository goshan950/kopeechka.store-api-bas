<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"pafpxbsy", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
<%= _.template($('#variable_constructor').html())({id:"Save", description:tr("Variable to store your balance"), default_variable: "BALANCE", help: {description: tr("Your balance on kopeechka.store")}}) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
