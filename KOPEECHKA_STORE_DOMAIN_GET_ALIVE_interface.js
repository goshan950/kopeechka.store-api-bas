<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"abygsitr", description:tr("Domain"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Domain which lifetime you want to know")} }) %>
<%= _.template($('#variable_constructor').html())({id:"Save", description:tr("Variable to store domain lifetime"), default_variable: "ALIVE", help: {description: tr("Displayed in days. Domain zones ml, tk, gq, cf, gk, ga can end at any time")}}) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">The time period in which the domain is available for reuse</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
