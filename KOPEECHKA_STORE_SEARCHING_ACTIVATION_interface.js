<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"vejscrjx", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Leave this field unchanged, if you have no site")} }) %>
<%= _.template($('#input_constructor').html())({id:"bjlkgbeu", description:tr("Comment text"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Enter comment text to search via it")} }) %>
<%= _.template($('#input_constructor').html())({id:"hsqpulpw", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
<%= _.template($('#variable_constructor').html())({id:"Save", description:tr("Variable to store email"), default_variable: "EMAIL", help: {description: tr("Kopeechka.store email")}}) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">You can only find <b>ONE</b> email at a time</div>
<div class="tr tooltip-paragraph-fold">If you have the same comment on several emails the last emails will be taken</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
