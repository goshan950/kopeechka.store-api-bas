<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"urhasnux", description:tr("Email"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "[[EMAIL]]", help: {description: tr("Enter the variable with kopeechka.store email")} }) %>
<%= _.template($('#input_constructor').html())({id:"qneugjvz", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("The site, which was used for email")} }) %>
<%= _.template($('#input_constructor').html())({id:"kfcngdud", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">This action will cancel your activation and return the money to the balance OR move your activation to history</div>
<div class="tr tooltip-paragraph-fold">We recommended to do this action at the end of the template or after successfully receiving the letter</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
