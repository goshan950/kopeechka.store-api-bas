<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"gwyaodot", description:tr("Email type"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "rambler.ru,mail.com,hotmail.com", help: {description: tr("You may type other email types here. For example: outlook.com, list.ru, gmx.com etc")} }) %>
<%= _.template($('#input_constructor').html())({id:"wlvamaku", description:tr("The site for registration"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("For example: facebook.com, tiktok.com, instagram.com")} }) %>
<%= _.template($('#input_constructor').html())({id:"ihhnoatx", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
<%= _.template($('#variable_constructor').html())({id:"Save", description:tr("Variable to store email"), default_variable: "EMAIL", help: {description: tr("Kopeechka.store email")}}) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">All available mail types you can see <a target="_blank" href="https://kopeechka.store/forums_img/get_prices.gif">HERE</a></div>
<div class="tr tooltip-paragraph-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-fold">What you can write in mail_type field:</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-fold">* <b>YANDEX</b> - Only yandex domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>OUTLOOK</b> - Only outlook domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>MAILCOM</b> - Only mail.com domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>MAILRU</b> - Only mail.ru domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>RAMBLER</b> - Only rambler domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>ALL</b> - Only our custom domains will taken</div>
<div class="tr tooltip-paragraph-fold">* <b>REAL</b> - All real domains (like outlook.com,mail.com) will taken</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">Also you can take specific email. Just write it full domain (<b>mail.ru</b>,<b>rambler.ru</b> etc)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
