<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"lbwcjksy", description:tr("Domain"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "mail.ru", help: {description: tr("Domain which you want to delete from Blacklist")} }) %>
<%= _.template($('#input_constructor').html())({id:"aamcdzhr", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "instagram.com", help: {description: tr("The site for which you want to remove the domain from the blacklist")} }) %>
<%= _.template($('#input_constructor').html())({id:"vbahkbnz", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">Removing a domain from the blacklist</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
