<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"rlgmdfny", description:tr("Email"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "[[EMAIL]]", help: {description: tr("Enter the variable with kopeechka.store email")} }) %>
<%= _.template($('#input_constructor').html())({id:"nyckogpt", description:tr("Regular expression"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "0", help: {description: tr("Leave this field unchanged, if you have no regular expression")} }) %>
<%= _.template($('#input_constructor').html())({id:"elwboeqg", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("The site, which was used for email")} }) %>
<%= _.template($('#input_constructor').html())({id:"auytvoyr", description:tr("Times"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "12", help: {description: tr("How many times to look for a letter.\n1 time = 5 seconds.\nWe recommend to stay it unchanged (12 times = 1 minute)")} }) %>
<%= _.template($('#input_constructor').html())({id:"bxtvawdc", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
<%= _.template($('#variable_constructor').html())({id:"Save", description:tr("Variable to store message / link"), default_variable: "MESSAGE", help: {description: tr("Link or full message")}}) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">Get message from email</div>
<div class="tr tooltip-paragraph-fold"><a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://youtu.be/ll1Nrs7rIGc'); return false;"><span class="tr">Video tutorial</span></a> for module using</div>
<div class="tr tooltip-paragraph-fold"><a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://bablosoft.github.io/RegexpConstructor/'); return false;"><span class="tr">Regular expression constructor</span></a></div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
