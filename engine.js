function KOPEECHKA_STORE_GET_BALANCE()
   {
   
      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/user-balance?token=" + VAR_TOKEN,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      VAR_BALANCE = JSON.parse(VAR_SAVED_CONTENT)["balance"]
      

      
      
      _function_return(VAR_BALANCE)
      

   }
   

function KOPEECHKA_STORE_SEARCHING_ACTIVATION()
   {
   
      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TEXT = _function_argument("TEXT")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      VAR_TEXT = encodeURIComponent(VAR_TEXT)
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-get-bulk?token=" + VAR_TOKEN + "\u0026count=1\u0026site=" + VAR_SITE + "\u0026comment=" + VAR_TEXT,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      _function_return("JSON.parse(" + VAR_SAVED_CONTENT + ")[\u0022items\u0022][0][\u0022email\u0022]")
      

   }
   

function KOPEECHKA_STORE_BLACKLIST_ADD_DOMAIN()
   {
   
      
      
      VAR_DOMAIN = _function_argument("DOMAIN")
      

      
      
      VAR_EXPIRE = _function_argument("EXPIRE")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/domain-add-blacklist?token=" + VAR_TOKEN + "\u0026domain=" + VAR_DOMAIN + "\u0026site=" + VAR_SITE + "\u0026expire=" + VAR_EXPIRE,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

   }
   

function KOPEECHKA_STORE_BLACKLIST_DOMAIN_EXCLUDE()
   {
   
      
      
      VAR_DOMAIN = _function_argument("DOMAIN")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/domain-exclude-blacklist?token=" + VAR_TOKEN + "\u0026domain=" + VAR_DOMAIN + "\u0026site=" + VAR_SITE,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

   }
   

function KOPEECHKA_STORE_SET_COMMENT()
   {
   
      
      
      VAR_EMAIL = _function_argument("EMAIL")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TEXT = _function_argument("TEXT")
      

      
      
      VAR_TEXT = encodeURIComponent(VAR_TEXT)
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-get-fresh-id?token=" + VAR_TOKEN + "\u0026site=" + VAR_SITE + "\u0026email=" + VAR_EMAIL,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      VAR_ID = JSON.parse(VAR_SAVED_CONTENT)["id"]
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-set-comment?token=" + VAR_TOKEN + "\u0026id=" + VAR_ID + "\u0026comment=" + VAR_TEXT + VAR_TEXT + VAR_TEXT + VAR_TEXT + VAR_TEXT + VAR_TEXT + VAR_TEXT,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

   }
   

function KOPEECHKA_STORE_DOMAIN_GET_ALIVE()
   {
   
      
      
      VAR_DOMAIN = _function_argument("DOMAIN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/domain-get-alive?domain=" + VAR_DOMAIN,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      VAR_ALIVE_TIME = JSON.parse(VAR_SAVED_CONTENT)["alive"]
      

      
      
      _function_return(VAR_ALIVE_TIME)
      

   }
   

function KOPEECHKA_STORE_REORDER_MESSAGE()
   {
   
      
      
      VAR_EMAIL = _function_argument("EMAIL")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-reorder?site=" + VAR_SITE + "\u0026email=" + VAR_EMAIL + "\u0026token=" + VAR_TOKEN,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      VAR_EMAIL = JSON.parse(VAR_SAVED_CONTENT)["mail"]
      

      
      
      _function_return(VAR_EMAIL)
      

   }
   

function KOPEECHKA_STORE_CANCEL_MESSAGE()
   {
   
      
      
      VAR_EMAIL = _function_argument("EMAIL")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-get-fresh-id?token=" + VAR_TOKEN + "\u0026site=" + VAR_SITE + "\u0026email=" + VAR_EMAIL,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

         
         
         _function_return(VAR_ERROR)
         

      })!
      

      
      
      VAR_ID = JSON.parse(VAR_SAVED_CONTENT)["id"]
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-cancel?id=" + VAR_ID + "\u0026token=" + VAR_TOKEN,{method:("GET"),headers:("")})!
      

   }
   

function KOPEECHKA_STORE_GET_MESSAGE()
   {
   
      
      
      VAR_TIMES = _function_argument("TIMES")
      

      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      VAR_EMAIL = _function_argument("EMAIL")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_REGEXP = _function_argument("REGEXP")
      

      
      
      _switch_http_client_main()
      http_client_get2("http://api.kopeechka.store/mailbox-get-fresh-id?token=" + VAR_TOKEN + "\u0026site=" + VAR_SITE + "\u0026email=" + VAR_EMAIL,{method:("GET"),headers:("")})!
      

      
      
      _switch_http_client_main()
      VAR_SAVED_CONTENT = http_client_encoded_content("auto")
      

      
      
      _if(JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK",function(){
      
         
         
         VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
         

      })!
      

      
      
      VAR_ID = JSON.parse(VAR_SAVED_CONTENT)["id"]
      

      
      
      _do(function(){
      VAR_CYCLE_INDEX = _iterator() - 1 + parseInt(1)
      if(VAR_CYCLE_INDEX > parseInt(VAR_TIMES))_break();
      
         
         
         _switch_http_client_main()
         http_client_get2("http://api.kopeechka.store/mailbox-get-message?full=1\u0026id=" + VAR_ID + "\u0026token=" + VAR_TOKEN,{method:("GET"),headers:("")})!
         

         
         
         _switch_http_client_main()
         VAR_SAVED_CONTENT = http_client_encoded_content("auto")
         

         
         
         IF_ELSE_EXPRESSION_446262 = JSON.parse(VAR_SAVED_CONTENT)["status"]!="OK";
         _if(IF_ELSE_EXPRESSION_446262,function(){
         
            
            
            _cycle_params().if_else = JSON.parse(VAR_SAVED_CONTENT)["value"]=="WAIT_LINK";
            _if(_cycle_params().if_else,function(){
            
               
               
               sleep(5000)!
               

            })!
            

            
            
            _if(!_cycle_params().if_else,function(){
            
               
               
               VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
               

               
               
               _function_return(VAR_ERROR)
               

            })!
            delete _cycle_params().if_else;
            

         })!
         

         
         
         _if(!IF_ELSE_EXPRESSION_446262,function(){
         
            
            
            _cycle_params().if_else = VAR_REGEXP=="0";
            _if(_cycle_params().if_else,function(){
            
               
               
               _cycle_params().if_else = !JSON.parse(VAR_SAVED_CONTENT)["value"];
               _if(_cycle_params().if_else,function(){
               
                  
                  
                  VAR_MESSAGE = JSON.parse(VAR_SAVED_CONTENT)["fullmessage"]
                  

                  
                  
                  _break("function")
                  

               })!
               

               
               
               _if(!_cycle_params().if_else,function(){
               
                  
                  
                  VAR_MESSAGE = JSON.parse(VAR_SAVED_CONTENT)["value"]
                  

                  
                  
                  _break("function")
                  

               })!
               delete _cycle_params().if_else;
               

            })!
            

            
            
            _if(!_cycle_params().if_else,function(){
            
               
               
               var regexp_result = native("regexp", "first", JSON.stringify({text: JSON.parse(VAR_SAVED_CONTENT)["fullmessage"],regexp:VAR_REGEXP}))
               if(regexp_result.length == 0)
               regexp_result = []
               else
               regexp_result = JSON.parse(regexp_result)
               VAR_MESSAGE = regexp_result[0]
               

               
               
               _break("function")
               

            })!
            delete _cycle_params().if_else;
            

         })!
         delete IF_ELSE_EXPRESSION_446262;
         

      })!
      

      
      
      _function_return(VAR_MESSAGE)
      

   }
   

function KOPEECHKA_STORE_GET_EMAIL()
   {
   
      
      
      VAR_TOKEN = _function_argument("TOKEN")
      

      
      
      VAR_SITE = _function_argument("SITE")
      

      
      
      VAR_MAIL_TYPE = _function_argument("MAIL_TYPE")
      

      
      
      VAR_SOFT_ID = "2"
      

      
      
      _cycle_params().if_else = VAR_MAIL_TYPE != "ALL";
      _if(_cycle_params().if_else,function(){
      
         
         
         VAR_REQUEST = "site=" + VAR_SITE + "\u0026mail_type=" + VAR_MAIL_TYPE + "\u0026token=" + VAR_TOKEN + "\u0026soft=" + VAR_SOFT_ID
         

      })!
      

      
      
      _if(!_cycle_params().if_else,function(){
      
         
         
         VAR_REQUEST = "site=" + VAR_SITE + "\u0026token=" + VAR_TOKEN + "\u0026soft=" + VAR_SOFT_ID
         

      })!
      delete _cycle_params().if_else;
      

      
      
      VAR_MAIL_TRY = "3"
      

      
      
      VAR_CYRCLE = "0"
      

      
      
      _do(function(){
      VAR_CYCLE_INDEX = _iterator() - 1
      BREAK_CONDITION = true;
      if(!BREAK_CONDITION)_break();
      
         
         
         _call(function()
         {
         _on_fail(function(){
         VAR_LAST_ERROR = _result()
         VAR_ERROR_ID = ScriptWorker.GetCurrentAction()
         VAR_WAS_ERROR = false
         _break(1,true)
         })
         CYCLES.Current().RemoveLabel("function")
         
            
            
            _switch_http_client_main()
            general_timeout_next(5000)
            http_client_get_no_redirect2("http://api.kopeechka.store/mailbox-get-email?" + VAR_REQUEST,{method:("GET"),headers:("")})!
            

         },null)!
         

         
         
         VAR_CYRCLE = parseInt(VAR_CYRCLE) + parseInt(1)
         

         
         
         _if(VAR_WAS_ERROR,function(){
         
            
            
            sleep(1000)!
            

            
            
            _next("function")
            

         })!
         

         
         
         _switch_http_client_main()
         VAR_SAVED_CONTENT = http_client_encoded_content("auto")
         

         
         
         VAR_STATUS = JSON.parse(VAR_SAVED_CONTENT)["status"]
         

         
         
         _cycle_params().if_else = VAR_STATUS=="OK";
         _if(_cycle_params().if_else,function(){
         
            
            
            VAR_SRKME = JSON.parse(VAR_SAVED_CONTENT)["mail"]
            

            
            
            _break("function")
            

         })!
         

         
         
         _if(!_cycle_params().if_else,function(){
         
            
            
            VAR_ERROR = JSON.parse(VAR_SAVED_CONTENT)["value"]
            

            
            
            _cycle_params().if_else = VAR_ERROR=="Закончились почты // This domain is out of stock.";
            _if(_cycle_params().if_else,function(){
            
               
               
               _if(VAR_CYRCLE > VAR_MAIL_TRY,function(){
               
                  
                  
                  _function_return(VAR_ERROR)
                  

               })!
               

               
               
               sleep(1000)!
               

            })!
            

            
            
            _if(!_cycle_params().if_else,function(){
            
               
               
               _function_return(VAR_ERROR)
               

            })!
            delete _cycle_params().if_else;
            

         })!
         delete _cycle_params().if_else;
         

      })!
      

      
      
      _function_return(VAR_SRKME)
      

   }
   

