<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"lbwcjksy", description:tr("Domain"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "mail.ru", help: {description: tr("Domain to add to the Blacklist")} }) %>
<%= _.template($('#input_constructor').html())({id:"uzadwqqf", description:tr("Removal date from the Blacklist"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "0", help: {description: tr("Stay this field unchanged, if you want the domain to be blacklisted indefinitely.\nIf the domain needs to be added to the blacklist for a specific time, specify the end date in UNIXTIME format.")} }) %>
<%= _.template($('#input_constructor').html())({id:"aamcdzhr", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "instagram.com", help: {description: tr("The site to which the blacklist belongs")} }) %>
<%= _.template($('#input_constructor').html())({id:"vbahkbnz", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">The selected domains will not be used for registration on the specified site.</div>
<div class="tr tooltip-paragraph-fold">For example, in case of a standard setting, mail.ru mail will not be taken for the instagram.com site.</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">You can get time in <b>UNIXTIME</b> format <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://www.unixtimestamp.com/'); return false;"><span class="tr">HERE</span></a>.</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
