<div class="container-fluid">
<%= _.template($('#input_constructor').html())({id:"xvejxulz", description:tr("Email"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "[[EMAIL]]", help: {description: tr("Enter the variable with kopeechka.store email")} }) %>
<%= _.template($('#input_constructor').html())({id:"nyredomv", description:tr("Site"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("The site, which was used for email")} }) %>
<%= _.template($('#input_constructor').html())({id:"tmfvkucx", description:tr("Comment text"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Comment text to be used for search")} }) %>
<%= _.template($('#input_constructor').html())({id:"siwvinxv", description:tr("API key"), default_selector: "string", disable_expression:true, disable_int:true, value_string: "", help: {description: tr("Your API key from kopeechka.store. You can find it on \"My profile\" page")} }) %>
</div>
<div class="tooltipinternal">
<div class="tr tooltip-paragraph-first-fold">You can group emails via commenting</div>
<div class="tr tooltip-paragraph-fold"></div>
<div class="tr tooltip-paragraph-last-fold">The tutorial, where to find API key on <a href="#" onclick="BrowserAutomationStudio_OpenUrl('https://kopeechka.store'); return false;"><span class="tr">kopeechka.store</span></a> (<a target="_blank" href="https://kopeechka.store/forums_img/find_token.gif">TAP HERE</a>)</div>
</div>
<%= _.template($('#back').html())({action:"executeandadd", visible:true}) %>
